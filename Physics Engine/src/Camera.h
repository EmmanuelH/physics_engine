/** \file Camera.h
	Camera class taken from learnopengl.com
	Temporary until I'm not lazy.

	Author: learnopengl.com
*/
#pragma once

#include <QVector>
#include <QOpenGLFunctions_4_3_Core>
#include <QMath.h>
#include <QMatrix4x4>

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};

// Default camera values
const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 0.05f;
const GLfloat SENSITIVTY = 0.05f;
const GLfloat ZOOM = 45.0f;

const GLfloat VERTICALANGLE = 90;
const GLfloat ASPECTRATIO = 16.0f/ 9.0f;
const GLfloat NEARPLANE = 0.1f;
const GLfloat FARPLANE = 10000.0f;

// An abstract camera class that processes input and calculates the corresponding Eular Angles, Vectors and Matrices for use in OpenGL
class Camera
{
public:
	// Camera Attributes
	QVector3D Position;
	QVector3D Front;
	QVector3D Up;
	QVector3D Right;
	QVector3D WorldUp;

	// Eular Angles
	GLfloat Yaw;
	GLfloat Pitch;
	// Camera options
	GLfloat MovementSpeed;
	GLfloat MouseSensitivity;
	GLfloat Zoom;
	
	// Projection matrix settings
	GLfloat VerticalAngle;
	GLfloat AspectRatio;
	GLfloat NearPlane;
	GLfloat FarPlane;

	/// View buffer object location
	GLuint VBO;
	/// View buffer object location
	GLuint PBO;

	// Constructor with vectors
	Camera(QVector3D position = QVector3D(0.0f, 0.0f, 0.0f), QVector3D up = QVector3D(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH,
		GLfloat verticalAngle = VERTICALANGLE, GLfloat aspectRatio = ASPECTRATIO, GLfloat nearPlane = NEARPLANE, GLfloat farPlane = FARPLANE) :
		Front(QVector3D(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVTY), Zoom(ZOOM)
	{
		this->Position = position;
		this->WorldUp = up;
		this->Yaw = yaw;
		this->Pitch = pitch;
		this->VerticalAngle = verticalAngle;
		this->AspectRatio = aspectRatio;
		this->NearPlane = nearPlane;
		this->FarPlane = farPlane;
		this->updateCameraVectors();
	}

	// Constructor with scalar values
	Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch) :
		Front(QVector3D(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVTY), Zoom(ZOOM)
	{
		this->Position = QVector3D(posX, posY, posZ);
		this->WorldUp = QVector3D(upX, upY, upZ);
		this->Yaw = yaw;
		this->Pitch = pitch;
		this->updateCameraVectors();
	}

	// Returns the view matrix calculated using Eular Angles and the LookAt Matrix
	QMatrix4x4 GetViewMatrix()
	{
		QMatrix4x4 matrixToSend;
		matrixToSend.lookAt(this->Position, this->Position + this->Front, this->Up);
		return matrixToSend;
	}

	// Returns the projection matrix.
	QMatrix4x4 GetProjectionMatrix()
	{
		QMatrix4x4 matrixToSend;
		matrixToSend.perspective(VerticalAngle, AspectRatio, NearPlane, FarPlane);
		return matrixToSend;
	}

	// Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
	void ProcessKeyboard(Camera_Movement direction, GLfloat deltaTime)
	{
		GLfloat velocity = this->MovementSpeed * deltaTime;
		if (direction == FORWARD)
			this->Position += this->Front * velocity;
		if (direction == BACKWARD)
			this->Position -= this->Front * velocity;
		if (direction == LEFT)
			this->Position -= this->Right * velocity;
		if (direction == RIGHT)
			this->Position += this->Right * velocity;
	}

	// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
	void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset, GLboolean constrainPitch = true)
	{
		xoffset *= this->MouseSensitivity;
		yoffset *= this->MouseSensitivity;

		this->Yaw += xoffset;
		this->Pitch += yoffset;

		// Make sure that when pitch is out of bounds, screen doesn't get flipped
		if (constrainPitch)
		{
			if (this->Pitch > 89.0f)
				this->Pitch = 89.0f;
			if (this->Pitch < -89.0f)
				this->Pitch = -89.0f;
		}

		// Update Front, Right and Up Vectors using the updated Eular angles
		this->updateCameraVectors();
	}

	// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
	void ProcessMouseScroll(GLfloat yoffset)
	{
		if (this->Zoom >= 1.0f && this->Zoom <= 45.0f)
			this->Zoom -= yoffset;
		if (this->Zoom <= 1.0f)
			this->Zoom = 1.0f;
		if (this->Zoom >= 45.0f)
			this->Zoom = 45.0f;
	}

private:
	// Calculates the front vector from the Camera's (updated) Eular Angles
	void updateCameraVectors()
	{
		// Calculate the new Front vector
		QVector3D front;
		front.setX(qCos(qDegreesToRadians(this->Yaw)) * qCos(qDegreesToRadians(this->Pitch)));
		front.setY(qSin(qDegreesToRadians(this->Pitch)));
		front.setZ(qSin(qDegreesToRadians(this->Yaw)) * qCos(qDegreesToRadians(this->Pitch)));
		this->Front = front.normalized();
		
		// Also re-calculate the Right and Up vector
		// Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
		this->Right = QVector3D::normal(this->Front, this->WorldUp);  
		this->Up = QVector3D::normal(this->Right, this->Front);
	}
};