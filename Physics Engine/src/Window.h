/**	\file Window.h
Opens Window and creates OpenGL context

Author: Emmanuel Harley
*/
#pragma once

#include <QWindow>
#include <qguiapplication.h>
#include <QOpenGLFunctions_4_3_Core>
#include <QtCore\QCoreApplication>
#include <QtGui\QOpenGLContext>
#include <QtGui\QOpenGLPaintDevice>
#include <QtGui\QPainter>

namespace os{
	/// Opens the window and creates an OpenGL context
	class Window :
		public QWindow,
		protected QOpenGLFunctions_4_3_Core
	{
		Q_OBJECT
	public:
		explicit Window(QWindow *parent = 0);

		virtual void render(QPainter *painter);
		virtual void render();
		virtual void main() = 0;

		virtual void initialize();

		void setAnimating(bool animating);
	public slots:
		void renderLater();
		void renderNow();

	protected:
		bool event(QEvent *event) Q_DECL_OVERRIDE;
		void exposeEvent(QExposeEvent *event) Q_DECL_OVERRIDE;
		void focusInEvent(QFocusEvent *event) Q_DECL_OVERRIDE;
		void focusOutEvent(QFocusEvent *event) Q_DECL_OVERRIDE;

	private:
		bool updatePending;
		bool animating;
		int startingTime;

		QOpenGLContext *glContext;
		QOpenGLPaintDevice *paintDevice;
	};
}