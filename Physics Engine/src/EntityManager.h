/** \file EntityManager.h
	Handles all things on screen

	Author: Emmanuel Harley
*/
#pragma once

#include "Mesh.h"
#include "Collider.h"
#include "BlenderParser.h"

#include <iostream>
using namespace std;

#include <QtMath>

namespace os{
	class GraphicsEngine;
	class RigidBody;
	/// Class that controls all entites in the class
	enum Entity{
		ENTITY_ICO_SPHERE, ENTITY_SPHERE, ENTITY_CUBE, ENTITY_PLANE, ENTITY_MONKEY
	};
	class EntityManager{
	public:
		EntityManager();
		
		void CollisionCheck();
		void Move(GLuint deltaT);
		void AddEntity(Entity entity, QMatrix4x4 *modelMatrix, RigidBody *rigidBody);

		/// Updates our entities.
		void Update(GLuint deltaT){
			CollisionCheck();
			Move(deltaT);
		}
		
		/// skip the whole entities part... kinda tedious.
		Mesh operator[](int i){
			return entities[i];
		}
		
		QVector<Mesh> entities;
	};
}