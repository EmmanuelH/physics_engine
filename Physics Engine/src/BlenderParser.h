/**	\file BlenderParser.h
Parses blenders .obj files for 3D models.

Author: Emmanuel Harley
*/

#pragma once

#include "Mesh.h"

#include <QtCore\QVector>
#include <QtCore\qdir>
#include <QtCore\qfile>
#include <QtCore\qstring>
#include <QtCore\qtextstream>
#include <QtGui\QVector3D>
#include <qtransform.h>
#include <QOpenGL.h>

namespace os{
	/// Parses Blender .obj files holds returns model info
	class ObjParser
	{
	public:
		/// constructor
		ObjParser(){
			Objects = new QVector<os::Mesh>();
		}

		void Parse(QString &filename);
		QVector<Mesh> *getObjects();
		Mesh getObject(int index);

		bool Convert(QString &filename);
		bool Convert();
	private:
		QVector3D stringToVector3D(QString &sVector);
		void stringToVN(QString &sOrder, os::VN_Index vnOrder[3]);


		/// File to parse for Mesh info
		QFile *file;

		/// List of Mesh objects
		QVector<Mesh> *Objects;
	};
}