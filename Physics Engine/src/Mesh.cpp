/**	\file Mesh.h
	Defines the vertices, textures, and the rest of the
	objects assets.
*/

#include "Mesh.h"

/** Get the position of an instance of the mesh
	The first three rows in the fourth column of the matrix model
	\param index The index of the instance of the mesh to access
	\returns The position of the mesh
*/
QVector3D os::Mesh::getPosition(GLuint index){
	return modelMatrices[index].column(3).toVector3D();
}

/** Gets the scale of the instance
	Scale is contained in the fourth column, fourth row, of the model matrix.
	\returns The scale of the instance
*/
GLfloat os::Mesh::getScale(GLuint index){
	return modelMatrices[index].column(3)[3];
}

/** Get the vertex Array object bound to this mesh
	\returns The location of the vertex array object
*/
GLuint *os::Mesh::getVAO(){
	return &VAO;
}

/** Get the vertex Buffer Object bound to this mesh
	\returns The location of the vertex buffer object	
*/
GLuint *os::Mesh::getVBO(){
	return &VBO;
}

/** Get the Model matrix buffer object
	\returns The location of the model matrix buffer object
*/
GLuint *os::Mesh::getMBO(){
	return &MBO;
}

/** Get the Element Buffer Object bound to this mesh
	\returns The location of the element buffer object
*/
GLuint *os::Mesh::getEBO(){
	return &EBO;
}