/** \file EntityManager.cpp
	Handles all things on screen

	Author: Emmanuel Harley
*/
#include "EntityManager.h"

/// Constructor
os::EntityManager::EntityManager()
{
	// setup ico sphere
	ObjParser *parser = new ObjParser();
	Mesh *obj = new Mesh();
	parser->Parse(QStringLiteral("Resources/objects/ico sphere/ico sphere.obj"));
	*obj = parser->getObject(0);
	obj->collider = new SphereCollider(COLLIDER_SPHERE);
	Collider::FindAABB(obj);
	Collider::FindRadius(obj);
	entities.append(*obj);
	delete parser;
	
	// setup sphere
	parser = new ObjParser();
	obj = new Mesh();
	parser->Parse(QStringLiteral("Resources/objects/sphere/sphere.obj"));
	*obj = parser->getObject(0);
	obj->collider = new SphereCollider(COLLIDER_SPHERE);
	Collider::FindAABB(obj);
	Collider::FindRadius(obj);
	entities.append(*obj);
	delete parser;

	// setup cube
	parser = new ObjParser();
	obj = new Mesh();
	parser->Parse(QStringLiteral("Resources/objects/cube/cube.obj"));
	*obj = parser->getObject(0);
	obj->collider = new AABBCollider(COLLIDER_AABB);
	Collider::FindAABB(obj);
	Collider::FindRadius(obj);
	entities.append(*obj);
	delete parser;

	// setup plane
	parser = new ObjParser();
	obj = new Mesh();
	parser->Parse(QStringLiteral("Resources/objects/plane/plane.obj"));
	*obj = parser->getObject(0);
	obj->collider = new PlaneCollider(COLLIDER_PLANE);
	Collider::FindAABB(obj);
	Collider::FindRadius(obj);
	entities.append(*obj);
	delete parser;

	// setup monkeys
	/*parser = new ObjParser();
	obj = new Mesh();
	parser->Parse(QStringLiteral("Resources/objects/monkey/monkey.obj"));
	*obj = parser->getObject(0);
	obj->collider = new SphereCollider(COLLIDER_SPHERE);
	Collider::FindAABB(obj);
	Collider::FindRadius(obj);
	entities.append(*obj);
	delete parser;*/
}

/** Add a new instance to the list of mesh objects
	
	\param entity The type of entity you want to add
	\param modelMatrix The model matrix of the new instance.
*/
void os::EntityManager::AddEntity(Entity entity, QMatrix4x4 *modelMatrix, RigidBody *rigidBody)
{
	entities[entity].modelMatrices.append(*modelMatrix);
	entities[entity].rigidbodies.append(*rigidBody);
}

/** Check for collsions for every object

*/
void os::EntityManager::CollisionCheck()
{

}

/** Apply new forces acting on object.

*/
void os::EntityManager::Move(GLuint deltaT)
{
	for (int i = 0; i < entities.count(); i++)
	{
		for (int j = 0; j < entities[i].rigidbodies.count(); j++)
		{
			// make sure its atleast moving before we do this...
			if (entities[i].rigidbodies[j].getVelocity().length() >= 1)
			{
				GLdouble deltaTime = deltaT / 1000.0;
				QVector3D velocity = entities[i].rigidbodies[j].getVelocity();
				QVector3D accel = entities[i].rigidbodies[j].getAcceleration();
				// Use kinematics in order to find the distance traveled
				QVector3D deltaPosition = velocity*deltaTime + (accel * qPow(deltaTime, 2)) / 2.0f;
				
				// set new velocity
				entities[i].rigidbodies[j].setVelocity(velocity + accel*deltaTime);
				
				// Translate model matrix for that instance
				entities[i].modelMatrices[j].translate(deltaPosition);
			}
		}
	}
}