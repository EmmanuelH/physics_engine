/**	\file RigidBody.h
	Contains everything physics-y about an object.
*/
#pragma once

#include <QVector3D>
#include <QOpenGLFunctions_4_3_core>

namespace os{
	class RigidBody {
	public:
		RigidBody();
		/// destructor
		~RigidBody() {
		}
		
		// Velocity modifiers
		QVector3D getVelocity();
		void setVelocity(QVector3D vel);

		// Acceleration modifiers
		QVector3D getAcceleration();
		void setAcceleration(QVector3D acc);

		// Mass modifiers
		GLfloat getMass();
		void setMass(GLfloat _mass);

		// Center mass modifiers
		QVector3D getCenterMass();
		void setCenterMass(QVector3D _centerMass);
	private:
		/// Velocity of our rigid body
		QVector3D velocity;
		/// Acceleration of our rigid body
		QVector3D acceleration;

		/// Mass of the rigid body
		GLfloat mass;

		/// Center mass location
		QVector3D centerMass;
	};
}