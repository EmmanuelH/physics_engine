/**	\file RigidBody.cpp
	rigid bodies
*/
#include "RigidBody.h"

/** Constructor
*/
os::RigidBody::RigidBody() :
centerMass(0, 0, 0),
mass(0),
velocity(0, 0, 0){
}

/** Get the current velocity vector of the rigid body
	\returns the velocity vector of the rigid body
*/
QVector3D os::RigidBody::getVelocity(){
	return velocity;
}

/**	Sets the new velocity vector
	\param vel The new velocity vector
*/
void os::RigidBody::setVelocity(QVector3D vel){
	velocity = vel;
}

/** Get the current acceleration vector of the rigid body
	\returns the acceleration vector of the rigid body
*/
QVector3D os::RigidBody::getAcceleration(){
	return acceleration;
}

/**	Sets the new acceleration vector
	\param acc The new acceleration vector
*/
void os::RigidBody::setAcceleration(QVector3D acc){
	acceleration = acc;
}

/** Get the current mass of the rigid body
	\returns the mass of the rigid body
*/
GLfloat os::RigidBody::getMass(){
	return mass;
}

/**	Sets the new mass
	\param vel The new mass
*/
void os::RigidBody::setMass(GLfloat _mass){
	mass = _mass;
}

/** Get the current center mass vector of the rigid body
	\returns the center mass vector of the rigid body
*/
QVector3D os::RigidBody::getCenterMass(){
	return centerMass;
}

/**	Sets the new acceleration vector
	\param _centerThe new acceleration vector
*/
void os::RigidBody::setCenterMass(QVector3D _centerMass){
	centerMass = _centerMass;
}