/**	\file Mesh.h
	Defines the vertices, textures, and the rest of the
	objects assets.
*/
#pragma once

#include "RigidBody.h"
#include <QMatrix4x4>
#include <QtCore\QVector>
#include <QtCore\QString>
#include <QOpenGLFunctions_4_3_Core>
#include <QtGui\QVector3D>

namespace os
{
	class Collider;
	/// vertex/face and normal draw order
	struct VN_Index{
		GLuint vIndex; /// vertex index for draw order of face
		GLuint nIndex; /// normal index for vertex
	};

	/** Mesh
		Defines the vertices, normals, textures, and the rest of the
		objects assets.
	*/
	class Mesh
	{
	public:
		/// constructor
		Mesh() {
		}

		/// Destructor
		~Mesh(){
			Clear();
		}

		/// Clear all vectors
		void Clear(){
			Vertices.clear();
			Normals.clear();
			vnOrder.clear();
			modelMatrices.clear();
			rigidbodies.clear();
		}

		QVector3D getPosition(GLuint index);
		GLfloat getScale(GLuint index);

		GLuint *os::Mesh::getVAO();
		GLuint *os::Mesh::getVBO();
		GLuint *os::Mesh::getMBO();
		GLuint *os::Mesh::getEBO();

		/// Our list of rigid bodies
		QVector<RigidBody> rigidbodies;

		/// Our model matrix for each instance of the mesh
		QVector<QMatrix4x4> modelMatrices;

		/// Our collider
		Collider *collider;

		/// Object Name
		QString objName;

		/// List of objects vertices
		QVector<QVector3D> Vertices;
		/// List of objects normals
		QVector<QVector3D> Normals;
		/// Draw order of objects vertices and normals
		QVector<VN_Index> vnOrder;
	private:
		// Opengl object ID's. Keeps track of what Mesh we are talking about.
		/// Vertex Array object bound to this mesh
		GLuint VAO;
		/// Vertex Buffer Object bound to this mesh
		GLuint VBO;
		/// Model matrix buffer object
		GLuint MBO;
		/// Element Buffer Object bound to this mesh
		GLuint EBO;
	};
}