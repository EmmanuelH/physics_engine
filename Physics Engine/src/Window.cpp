/**	\file Window.cpp
Window class definition

Author: Emmanuel Harley
*/

#include "Window.h"

/** Constructor... initializes stuff
	
	\param parent The window object to be used for opengl rendering
*/
os::Window::Window(QWindow *parent)
	: QWindow(parent),
	updatePending(false),
	animating(false),
	glContext(0),
	paintDevice(0)
{
	setSurfaceType(QWindow::OpenGLSurface);
}

/** Initialize stuff
	virtual function....
*/
void os::Window::initialize()
{
}

/** does nothing

	\param painter Our much needed QPainter device
*/
void os::Window::render(QPainter *painter) {
	Q_UNUSED(painter);
}


/** Renders the scene
	virtual function....
*/
void os::Window::render()
{
	if (!paintDevice)
		paintDevice = new QOpenGLPaintDevice;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	paintDevice->setSize(size());

	QPainter painter(paintDevice);
	render(&painter);
}

/** request an update when busy

*/
void os::Window::renderLater()
{
	if (!updatePending) {
		updatePending = true;
		QCoreApplication::postEvent(this, new QEvent(QEvent::UpdateRequest));
	}
}

/** Initializes opengl context.

*/
void os::Window::renderNow()
{
	if (!isExposed())
		return;

	bool needsInitialize = false;

	if (!glContext){
		glContext = new QOpenGLContext(this);
		glContext->setFormat(requestedFormat());
		glContext->create();

		needsInitialize = true;
	}

	glContext->makeCurrent(this);

	if (needsInitialize){
		initializeOpenGLFunctions();
		initialize();
	}

	main();
	render();

	glContext->swapBuffers(this);

	if (animating)
		renderLater();
}

/** Handles Qt events

	\param event The event that was triggered
*/
bool os::Window::event(QEvent *event)
{
	switch (event->type()){
	case QEvent::UpdateRequest:
	{
		updatePending = false;
		renderNow();
		return true;
	}
	default:
		return QWindow::event(event);
	}
}

/** Handles the Windows expose event

	\param event The expose event
*/
void os::Window::exposeEvent(QExposeEvent *event)
{
	Q_UNUSED(event);

	if (isExposed())
		renderNow();
}

/** Handles event when the window gains focus
	
	\param event The focus event
*/
void os::Window::focusInEvent(QFocusEvent *event)
{
	QGuiApplication::setOverrideCursor(Qt::BlankCursor);
}

/** Handles event when the window loses focus

	\param event The focus event
*/
void os::Window::focusOutEvent(QFocusEvent *event)
{
	QGuiApplication::restoreOverrideCursor();
}

/** Begins animation
	
	\param animate Turn on animation or not.
*/
void os::Window::setAnimating(bool animate)
{
	animating = animate;

	if (animating)
		renderLater();
}