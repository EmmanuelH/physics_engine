/**	\file GraphicsEngine.cpp
Displays geometry on the gl window

Author: Emmanuel Harley
*/
#include "GraphicsEngine.h"

/// Constructor
os::GraphicsEngine::GraphicsEngine()
	:instanceShader(0),
	frame(0)
{
	lastTime = 0;
	timer = new QElapsedTimer();
	timer->start();
	accumulator = 0;
	currentTime = timer->elapsed();
	camera = new Camera(QVector3D(0, 20, 50));
	firstMouse = true;
	for (GLuint i = 0; i < 255; i++)
		keys[i] = GL_FALSE;

	position = new QVector4D(25, 0, -.25f, 1);
}

/** Our temporary main...
	
*/
void os::GraphicsEngine::main()
{
	//prints time elapsed ever second (in ms)
	/*if (timer->elapsed() % 1000 <= 10){
		cout << "time elapsed: " << timer->elapsed()
			<< "\tclock Type: " << timer->clockType() << endl;
	}*/
}

/** Initializes opengl by linking the shaders and their attributes.
	This also loads our blender geometry.
*/
void os::GraphicsEngine::initialize()
{
	// setup shaders
	
	// initialize our shaders
	instanceShader = new QOpenGLShaderProgram(this);

	// Add and compile files
	instanceShader->addShaderFromSourceFile(QOpenGLShader::Vertex, "Resources\\shaders\\vInstanceShader.glsl");
	instanceShader->addShaderFromSourceFile(QOpenGLShader::Fragment, "Resources\\shaders\\fShader.glsl");
	instanceShader->link();
	
	// get variable locations for objects, should be the same for both shaders...
	positionLoc = instanceShader->attributeLocation("position");
	colourLoc = instanceShader->attributeLocation("color");
	modelMatrixLoc = instanceShader->attributeLocation("model_matrix");
	viewMatrixLoc = instanceShader->uniformLocation("view_matrix");
	projectionMatrixLoc = instanceShader->uniformLocation("projection_matrix");

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glEnable(GL_CULL_FACE);

	// Generate a large list of semi-random model transformation matrices
	GLfloat radius = 150.0f;
	GLfloat offset = 25.0f;
	GLuint amount = 100000;
	for (GLuint i = 0; i < 100000; i++)
	{
		QMatrix4x4 model;
		RigidBody rigidBody;

		// dont move...
		rigidBody.setVelocity(QVector3D(0, 0, 0));

		// 1. Translation: Randomly displace along circle with radius 'radius' in range [-offset, offset]
		GLfloat angle = (GLfloat)i / (GLfloat)amount * 360.0f;
		GLfloat displacement = (qrand() % (GLint)(2 * offset * 100)) / 100.0f - offset;
		GLfloat x = sin(angle) * radius + displacement;
		displacement = (qrand() % (GLint)(2 * offset * 100)) / 100.0f - offset;
		GLfloat y = -2.5f + displacement * 0.4f; // Keep height of asteroid field smaller compared to width of x and z
		displacement = (qrand() % (GLint)(2 * offset * 100)) / 100.0f - offset;
		GLfloat z = cos(angle) * radius + displacement;
		model.translate(QVector3D(x, y, z));

		// 2. Scale: Scale between 0.05 and 0.25f
		GLfloat scale = (qrand() % 20) / 100.0f + 0.05;
		model.scale(scale);

		// 3. Rotation: add random rotation around a (semi)randomly picked rotation axis vector
		GLfloat rotAngle = (qrand() % 360);
		model.rotate(rotAngle, QVector3D(0.4f, 0.6f, 0.8f));

		// 4. Now add to list of matrices
		entities.AddEntity(ENTITY_ICO_SPHERE, &model, &rigidBody);
	}
	
	QMatrix4x4 modelMatrices;
	RigidBody rigidBody;
	modelMatrices.scale(10);
	entities.AddEntity(ENTITY_SPHERE, &modelMatrices, &rigidBody);

	for (int i = 0; i < entities.entities.count(); i++)
		if (entities.entities[i].modelMatrices.count())
			SetupBuffers(&(entities.entities[i]));
}

/** This function is called when a key is released.
	Also holds a movement example.
*/
void os::GraphicsEngine::keyReleaseEvent(QKeyEvent *event)
{
	switch(event->key())
	{
	case Qt::Key_W:{
		keys[Qt::Key_W] = GL_FALSE;
	}break;
	case Qt::Key_A:{
		keys[Qt::Key_A] = GL_FALSE;
	}break;
	case Qt::Key_S:{
		keys[Qt::Key_S] = GL_FALSE;
	}break;
	case Qt::Key_D:{
		keys[Qt::Key_D] = GL_FALSE;
	}break;
	case Qt::Key_Right: {
		position[0].setX(position[0].x() + 1);
	}break;
	case Qt::Key_Left: {
		position[0].setX(position[0].x() - 1);
	}break;
	case Qt::Key_Up: {
		position[0].setY(position[0].y() + 1);
	}break;
	case Qt::Key_Down: {
		position[0].setY(position[0].y() - 1);
	}break;
	case Qt::Key_O:	{
		if (indexCount < drawCount)
			indexCount += 3;
		else
			indexCount = 0;
		cout << "Vertex: " << indexCount
			<< "\tPolygon count: " << indexCount / 3
			<< "\tTotal: " << vertexCount / 3 << endl;
	}break;
	case Qt::Key_Y:	{
		if (indexCount > 0)
			indexCount -= 3;
		else
			indexCount = drawCount;
		cout << "Vertex: " << indexCount
			<< "\tPolygon count: " << indexCount / 3
			<< "\tTotal: " << vertexCount / 3 << endl;
	}break;
	case Qt::Key_U:	{
		cout << "Background colour changed to white." << endl;
		glClearColor(1, 1, 1, 1);
	}break;
	case Qt::Key_P:	{
		cout << "Background colour changed to red." << endl;
		glClearColor(1, 0, 0, 1);
	}break;
	case Qt::Key_R:{
		indexCount = 0;
		cout << "Index reset." << endl;
	}
	default:
		break;
	}
}

/** This function is called when a key is pressed.
	Also holds a movement example.
*/
void os::GraphicsEngine::keyPressEvent(QKeyEvent *event)
{
	switch (event->key())
	{
	case Qt::Key_W:{
		keys[Qt::Key_W] = GL_TRUE;
	}break;
	case Qt::Key_A:{
		keys[Qt::Key_A] = GL_TRUE;
	}break;
	case Qt::Key_S:{
		keys[Qt::Key_S] = GL_TRUE;
	}break;
	case Qt::Key_D:{
		keys[Qt::Key_D] = GL_TRUE;
	}break;
	case Qt::Key_Escape:{
		QWindow::close();
	}break;
	case Qt::Key_Right:{
		
	}break;
	}
}

/** This function is called when mouse movement is detected

*/
void os::GraphicsEngine::mouseMoveEvent(QMouseEvent *event)
{
	GLfloat xpos = event->globalX(), ypos = event->globalY();
	if (firstMouse)
	{
		lastPos.setX(xpos);
		lastPos.setY(ypos);
		firstMouse = false;
	}

	GLfloat xoffset = xpos - lastPos.x();
	GLfloat yoffset = lastPos.y() - ypos;

	QPoint glob = mapToGlobal(QPoint(width() / 2, height() / 2));

	/*cout << "offset: \tlast: \t\tglob: " << endl
		<< "x: " << xoffset << "\ty: " << yoffset
		<< "\tx: " << lastPos.x() << "\ty: " << lastPos.y()
		<< "\tx: " << glob.x() << "\ty: " << glob.y() << endl;*/

	camera->ProcessMouseMovement(xoffset, yoffset);
	QCursor::setPos(glob);
	lastPos = glob;
}

/** Temporary move funtion
*/
void os::GraphicsEngine::MoveCamera(){
	if (keys[Qt::Key_W])
		camera->ProcessKeyboard(FORWARD, deltaTime);
	else if (keys[Qt::Key_A])
		camera->ProcessKeyboard(LEFT, deltaTime);
	else if (keys[Qt::Key_S])
		camera->ProcessKeyboard(BACKWARD, deltaTime);
	else if (keys[Qt::Key_D])
		camera->ProcessKeyboard(RIGHT, deltaTime);
}

/** Renders our geometry

*/
void os::GraphicsEngine::render()
{
	const qreal retinaScale = devicePixelRatio();
	glViewport(0, 0, width() * retinaScale, height() * retinaScale);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	instanceShader->bind();

	instanceShader->setUniformValue(projectionMatrixLoc, camera->GetProjectionMatrix());
	instanceShader->setUniformValue(viewMatrixLoc, camera->GetViewMatrix());

	MoveCamera();

	// Draw objects
	//SendMatrixBuffers(&entities.entities[ENTITY_SPHERE]);
	
	/*for (int i = 0; i < entities.entities.count(); i++)
		for (int j = 0; j < entities.entities[i].modelMatrices.count(); j++)
			entities.entities[i].modelMatrices[j].translate(1, 1, 1);*/
	deltaTime = timer->elapsed() - lastTime;
	lastTime = timer->elapsed();

	double t = 0.0;
	const double dt = 50;

	double newTime = timer->elapsed();
	double frameTime = newTime - currentTime;
	currentTime = newTime;
	accumulator += frameTime;
	while (accumulator >= dt)
	{
		entities.Update(dt);
		accumulator -= dt;
		t += dt;
	}

	// Update objects
	for (int i = 0; i < entities.entities.count(); i++)
		if (entities.entities[i].modelMatrices.count())
			SendMatrixBuffers(&entities.entities[i]);
	

	// Draw objects
	for (int i = 0; i < entities.entities.count(); i++)
		if (entities.entities[i].modelMatrices.count())
			Draw(&entities.entities[i]);

	instanceShader->release();

	frame++;
}

/** Send matrix array to the gpu
	
	\param mesh The mesh of model matrices that you want to upload
*/
void os::GraphicsEngine::SendMatrixBuffers(Mesh *mesh)
{
	// Binding vertex array model matrix buffer objects
	glBindVertexArray(*mesh->getVAO());
	glBindBuffer(GL_ARRAY_BUFFER, *mesh->getMBO());

	// Send Model Matrices to GPU
	glBufferSubData(GL_ARRAY_BUFFER, 0, mesh->modelMatrices.count() * sizeof(QMatrix4x4), &(mesh->modelMatrices[0]));

	glBindVertexArray(0);
}

/** Setups the buffers for the mesh object
	With instances.

	\param mesh The object to make buffers for
	\param amount The amount of objects you want to draw on the screen
*/
void os::GraphicsEngine::SetupBuffers(Mesh *mesh)
{
	// seed random number generator for random colors
	QTime now = QTime::currentTime();
	qsrand(now.msec());
	int high = 100, low = 0;
	float percent = (qrand() % ((high + 1) - low) + low) / float(high);

	// how many draw calls are we making
	GLfloat *vertices = new GLfloat[mesh->Vertices.count() * 3];
	GLfloat *colours = new GLfloat[mesh->Vertices.count() * 3];
	GLuint *vertexIndex = new GLuint[mesh->vnOrder.count()];

	for (int i = 0, j = 0; i < mesh->Vertices.count(); i++)
		for (int m = 0; m < 3; m++)
			vertices[j++] = mesh->Vertices[i][m];

	for (int i = 0; i < mesh->vnOrder.count(); i++)
		vertexIndex[i] = mesh->vnOrder[i].vIndex - 1;

	// Generate colours
	for (int i = 0; i < mesh->Vertices.count() * 3; i++)
		colours[i] = (qrand() % ((high + 1) - low) + low) / float(high);

	// Set up the vertex attributes
	glGenVertexArrays(1, mesh->getVAO());
	glBindVertexArray(*mesh->getVAO());

	// Get Buffer ID for vertex and colour attriibutes, 
	// and then send the data to the GPU
	glGenBuffers(1, mesh->getVBO());
	glBindBuffer(GL_ARRAY_BUFFER, *mesh->getVBO());
	glBufferData(GL_ARRAY_BUFFER, (sizeof(GLfloat)*mesh->Vertices.count() * 3) * 2, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, positionLoc, sizeof(GLfloat)*mesh->Vertices.count() * 3, vertices);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat)*mesh->Vertices.count() * 3, 
		sizeof(GLfloat)*mesh->Vertices.count() * 3, colours);

	// Tell Opengl where they are in the shader and their size.
	glVertexAttribPointer(positionLoc, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glVertexAttribPointer(colourLoc, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid *)(sizeof(GLfloat)*mesh->Vertices.count() * 3));
	glEnableVertexAttribArray(positionLoc);
	glEnableVertexAttribArray(colourLoc);

	// Setting up model matrix buffer objects
	glGenBuffers(1, mesh->getMBO());
	glBindBuffer(GL_ARRAY_BUFFER, *mesh->getMBO());
	// Send Model Matrices to GPU
	glBufferData(GL_ARRAY_BUFFER, mesh->modelMatrices.count() * sizeof(QMatrix4x4), &(mesh->modelMatrices[0]), GL_DYNAMIC_DRAW);

	// Set attribute pointers for matrix (4 times vec4)
	glEnableVertexAttribArray(modelMatrixLoc);
	glVertexAttribPointer(modelMatrixLoc, 4, GL_FLOAT, GL_FALSE, sizeof(QMatrix4x4), (GLvoid*)0);
	glEnableVertexAttribArray(modelMatrixLoc + 1);
	glVertexAttribPointer(modelMatrixLoc + 1, 4, GL_FLOAT, GL_FALSE, sizeof(QMatrix4x4), (GLvoid*)(sizeof(QVector4D)));
	glEnableVertexAttribArray(modelMatrixLoc + 2);
	glVertexAttribPointer(modelMatrixLoc + 2, 4, GL_FLOAT, GL_FALSE, sizeof(QMatrix4x4), (GLvoid*)(2 * sizeof(QVector4D)));
	glEnableVertexAttribArray(modelMatrixLoc + 3);
	glVertexAttribPointer(modelMatrixLoc + 3, 4, GL_FLOAT, GL_FALSE, sizeof(QMatrix4x4), (GLvoid*)(3 * sizeof(QVector4D)));

	// Get a new matrix per instance
	glVertexAttribDivisor(modelMatrixLoc, 1);
	glVertexAttribDivisor(modelMatrixLoc + 1, 1);
	glVertexAttribDivisor(modelMatrixLoc + 2, 1);
	glVertexAttribDivisor(modelMatrixLoc + 3, 1);

	// Set up the element array buffer
	glGenBuffers(1, mesh->getEBO());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *mesh->getEBO());
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint)*mesh->vnOrder.count(), vertexIndex, GL_STATIC_DRAW);

	// Done (unbind the object's VAO)
	glBindVertexArray(0);
}

/** Draws the given amount of instances of the object

	The model must first be setup through the method "SetupBuffers"
	before it can be drawn to the screen.

	\todo Optimize by seeing how many go through the single shader for best results
	\param mesh The model you wish to draw
*/
void os::GraphicsEngine::Draw(Mesh *mesh)
{
	glBindVertexArray(*mesh->getVAO());

	if (mesh->modelMatrices.count() > 1)
		glDrawElementsInstanced(GL_TRIANGLES, mesh->vnOrder.count(), GL_UNSIGNED_INT, NULL, mesh->modelMatrices.count());
	else
		glDrawElements(GL_TRIANGLES, mesh->vnOrder.count(), GL_UNSIGNED_INT, NULL);
	
	glBindVertexArray(0);
}