/**	\file Collider.h
	Colliders
*/
#pragma once

#include "Mesh.h"
#include <QMatrix4x4>
#include <QtCore\QVector>
#include <QtCore\QString>
#include <QOpenGLFunctions_4_3_Core>
#include <QtGui\QVector3D>

namespace os{
	/// Type of collision we are dealing with
	typedef enum{
		COLLIDER_NONE, COLLIDER_SPHERE, COLLIDER_AABB, COLLIDER_PLANE
	}CollisionType;

	typedef struct{
		QVector3D max;
		QVector3D min;
	}AABB;

	typedef struct{
		CollisionType collisionType;
		AABB aabb;
		GLfloat radius;
	}ColliderProperties;

	/// Base class to handle colliders
	class Collider{
	public:
		/// constructor
		Collider(){
			colliderProperties.collisionType = COLLIDER_SPHERE;
			colliderProperties.radius = 0;
			colliderProperties.aabb.max = QVector3D(0, 0, 0);
			colliderProperties.aabb.min = QVector3D(0, 0, 0);
		}
		/// constructor
		Collider(CollisionType coll){
			colliderProperties.collisionType = coll;
			colliderProperties.radius = 0;
			colliderProperties.aabb.max = QVector3D(0, 0, 0);
			colliderProperties.aabb.min = QVector3D(0, 0, 0);
		}

		/// destructor
		~Collider(){
		}

		// radius modifiers
		GLfloat getRadius();
		void setRadius(GLfloat radius);

		// AABB modifiers
		AABB getAABB();
		void setAABB(AABB aabb);

		// Collision type modifiers
		CollisionType getCollisionType();
		void setCollisionType(CollisionType type);

		static void FindRadius(Mesh *obj);
		static void FindAABB(Mesh *obj);

		/** Tests whether or not the two objects are colliding
			\todo change this to matrix to get more accurate detections.
			\param collider Sphere type collider colliding with this collider
			\param thisPosition Position of the object that is calling the function
			\param otherPosition Position of the object colliding with this object
			\return Returns true if the two objects are colliding. False otherwise.
		*/
		virtual bool IsColliding(Mesh *mesh1, Mesh *mesh2, GLuint index1, GLuint index2) = 0;
	private:
		/// Contains th collider properties
		ColliderProperties colliderProperties;

	};

	/// Our sphere collider detection class
	class SphereCollider : public Collider
	{
	public:
		SphereCollider(CollisionType coll):
		Collider(coll){
		}

		bool IsColliding(Mesh *mesh1, Mesh *mesh2, GLuint index1, GLuint index2)
		{
			
			// check for what type of collider we're interacting with
			switch (mesh1->collider->getCollisionType())
			{
			case COLLIDER_SPHERE:{
				//calculate the radius to use 
				GLfloat combinedRadius = mesh1->collider->getRadius() + mesh2->collider->getRadius();

				// get distance
				GLfloat distance = qAbs((mesh1->getPosition(index1) - mesh2->getPosition(index2)).length());
				/// check if spheres are touching or inside eachother
				if (combinedRadius >= distance)
					return true;
				else
					return false;
			}break;
			case COLLIDER_AABB:{

			}break;
			case COLLIDER_PLANE:{

			}break;
			}
			return false;
		}
	};

	/// AABB Collider class
	class AABBCollider : public Collider{
	public:
		AABBCollider(CollisionType coll) :
			Collider(coll){
		}
		bool IsColliding(Mesh *mesh1, Mesh *mesh2, GLuint index1, GLuint index2)
		{
			AABB *aabb1 = &mesh1->collider->getAABB();
			AABB *aabb2 = &mesh2->collider->getAABB();

			// check for what type of collider we're interacting with
			switch (mesh1->collider->getCollisionType())
			{
			case COLLIDER_SPHERE:{

			}break;
			case COLLIDER_AABB:{
				//Check if Box1's max is greater than Box2's min and Box1's min is less than Box2's max
				return (aabb1->max.x() + mesh1->getPosition(index1).x() >= aabb2->min.x() + mesh2->getPosition(index2).x() &&
					aabb1->min.x() + mesh1->getPosition(index1).x() <= aabb2->max.x() + mesh2->getPosition(index2).x() &&
					aabb1->max.y() + mesh1->getPosition(index1).y() >= aabb2->min.y() + mesh2->getPosition(index2).y() &&
					aabb1->min.y() + mesh1->getPosition(index1).y() <= aabb2->max.y() + mesh2->getPosition(index2).y() &&
					aabb1->max.z() + mesh1->getPosition(index1).z() >= aabb2->min.z() + mesh2->getPosition(index2).z() &&
					aabb1->min.z() + mesh1->getPosition(index1).z() <= aabb2->max.z() + mesh2->getPosition(index2).z());
			}break;
			case COLLIDER_PLANE:{

			}break;
			}
			return false;
		}
	};

	/// Plane Collider class
	class PlaneCollider : public Collider{
	public:
		PlaneCollider(CollisionType coll) :
			Collider(coll){
		}
		bool IsColliding(Mesh *mesh1, Mesh *mesh2, GLuint index1, GLuint index2)
		{
			return false;
		}
	};
}