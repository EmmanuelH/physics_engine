/**	\file BlenderParser.cpp
Definitions of the ObjParse class

Author: Emmanuel Harley
*/

#include "BlenderParser.h"

/** Parses the .obj file specified

	\param filename Name of .obj file that cantains your object(s)
*/
void os::ObjParser::Parse(QString &filename)
{
	file = new QFile(filename);

	// open file
	if (!file->open(QIODevice::ReadOnly | QIODevice::Text))
		return;

	QTextStream fStream(file);

	QString line;
	// skip header
	for (int i = 0; i < 2; i++)
		line = fStream.readLine();

	bool first = false;
	qint32 objectCount = 0;

	while (!fStream.atEnd())
	{
		Mesh myObj;

		if (line[0] == 'o')
		{
			line.remove(0, 2);
			// get object name
			myObj.objName = line;

			line = fStream.readLine();
			first = true;
		}
		while (line[0] != 'o' && !fStream.atEnd())
		{
			// prevent parser from skipping first line
			if (first)
				first = false;
			else
				line = fStream.readLine();

			// get normal coordinates
			if (line[1] == 'n')
			{
				line.remove(0, 3);
				myObj.Normals.append(stringToVector3D(line));
			}
			// get vertices coordinates
			else if (line[0] == 'v')
			{
				line.remove(0, 2);
				myObj.Vertices.append(stringToVector3D(line));
			}

			// get faces and normal order
			else if (line[0] == 'f')
			{
				line.remove(0, 2);
				os::VN_Index temp[3];
				stringToVN(line, temp);

				for (qint8 i = 0; i < 3; i++)
					myObj.vnOrder.append(temp[i]);
			}
		}

		// add new object
		if (objectCount > 0)
		{
			Objects->append(myObj);
		}
		objectCount++;
	}

	// close the file
	file->close();

	delete file;
	file = 0;
}

/** Returns list of Blender objects

	\return list of blender objects
*/
QVector<os::Mesh> *os::ObjParser::getObjects()
{
	return Objects;
}

/** Returns a Blender object

	\param index which BlenderObj to return in the list.
	\return BlenderObj
*/
os::Mesh os::ObjParser::getObject(int index)
{
	return Objects->at(index);
}

/**

*/
bool os::ObjParser::Convert(QString &filename)
{
	// Parse file
	Parse(filename);

	return Convert();
}

/**

*/
bool os::ObjParser::Convert()
{
	int j = 0;
	const int objectCount = Objects->count();
	for (int l = 0; l < objectCount; l++)
	{

		// vertex and normal count
		const int voCount = Objects->at(l).vnOrder.count();
		const QVector3D *tempVert = Objects->at(l).Vertices.constData();
		GLfloat *vertices = new GLfloat[voCount * 3];
		const os::VN_Index *tempOrder = Objects->at(l).vnOrder.constData();

		// check each face winding order
		for (int i = 0; i < voCount; i += 3)
		{

			for (int m = 0; m < 3; m++)
			{
				vertices[j++] = tempVert[tempOrder[l].vIndex - 1][m];
			}
		}

		QString test = Objects->at(l).objName + QStringLiteral(".vert");

		// File control for conversion output.
		file = new QFile(test);
		QTextStream fStream(file);

		// open file
		if (!file->open(QIODevice::WriteOnly | QIODevice::Text))
			return false;

		/// \todo make precision more accurate.
		int vertCount = voCount * 3;
		for (int i = 0; i < vertCount; i++){
			fStream << QString::number(vertices[i], 'f', 6) << QStringLiteral(",");
		}

		// clear memory
		delete[] vertices;
		vertices = 0;

		file->close();
		delete file;
		file = 0;
	}
	return true;
}

/** Converts String to vector 3D

	\param sVector 'v' or 'vn' line in .obj file
	\return returns instance of QVector3D
*/
QVector3D os::ObjParser::stringToVector3D(QString &sVector)
{
	QStringList sVectors = sVector.split(" ");

	float xyz[3];
	bool error = false;

	for (qint8 i = 0; i < 3; i++)
	{
		xyz[i] = sVectors[i].toFloat(&error);
		if (!error)
			; // error
	}

	return QVector3D(xyz[0], xyz[1], xyz[2]);
}

/** Converts String to VN_Index Object

	\param sOrder 'f' line from .obj file
	\param vnOrder Sets the VN_Index objects to the given array
*/
void os::ObjParser::stringToVN(QString &sOrder, os::VN_Index *vnOrder)
{
	// split each vertex #/normal
	QStringList sVectors = sOrder.split(" ");

	bool error = false;
	for (qint8 i = 0; i < 3; i++)
	{
		// split the normal and vertex number
		QStringList vnTemp = sVectors[i].split("//");

		// assign vertex number
		vnOrder[i].vIndex = vnTemp[0].toInt(&error);
		if (!error)
			; // error

		// assign normal vertex for face
		vnOrder[i].nIndex = vnTemp[1].toInt(&error);
		if (!error)
			; // error
	}
}