/**	\file GraphicsEngine.h
	Displays geometry on the gl window

Author: Emmanuel Harley
*/
#pragma once

#include "Window.h"
#include "BlenderParser.h"
#include "Mesh.h"
#include "Camera.h"
#include "EntityManager.h"

#include <iostream>
using namespace std;

#include <QtGui\QOpenGLShaderProgram>
#include <QtGui\QScreen>
#include <QGlobal.h>
#include <QtCore\QTimezone.h>
#include <QInputEvent>
#include <QElapsedTimer>

namespace os {
	/// Draws geometry to the screen
	class GraphicsEngine : public Window
	{
	public:
		GraphicsEngine();

		void main();

		friend class EntityManager;

		void keyReleaseEvent(QKeyEvent *event);
		void keyPressEvent(QKeyEvent *event);
		void mouseMoveEvent(QMouseEvent *event);
		void initialize() Q_DECL_OVERRIDE;
		void render() Q_DECL_OVERRIDE;

	private:
		void SetupBuffers(Mesh *mesh);
		void SendMatrixBuffers(Mesh *mesh);
		void Draw(Mesh *mesh);

		void MoveCamera();

		// Keeps track of shader variable locations
		/// \todo Make an enum for a location array if its the same.... a struct may also be suitable
		GLuint positionLoc;				/// keeps track of the location of the position variable in the vshader
		GLuint colourLoc;				/// keeps track of the location of the colour variable in the vshader
		GLuint modelMatrixLoc;			/// keeps track of the location of the matrix variable in the vshader
		GLuint viewMatrixLoc;
		GLuint projectionMatrixLoc;

		/// Keeps Track of the amount of vertices 
		GLuint vertexCount;
		/// Keeps track of the face we are on
		GLuint indexCount;
		/// keeps track of the amount of draw commands we have
		GLuint drawCount;

		/// Keeps track of position
		QVector4D *position;

		/// Our colours vertice
		GLfloat *colours;
		/// Holds all vertices of the object
		GLfloat *vertices;
		/// Holds the vertex draw order
		GLushort *vertexIndex;
		
		Camera *camera;
		QPoint lastPos;
		GLboolean firstMouse;
		
		EntityManager entities;
		/// Helps us with our shader... much easier with this thing.
		QOpenGLShaderProgram *instanceShader;

		QVector3D velocity;

		GLboolean keys[255];

		/// Keeps track of which frame we are on
		GLuint frame;
		/// Keeps track of how long the window has been open
		QElapsedTimer *timer;
		GLuint lastTime;
		GLuint deltaTime;
		GLdouble accumulator;
		GLdouble currentTime;
	};
}