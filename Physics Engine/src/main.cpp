/**	\file main.cpp
Where my main lies.

Author: Emmanuel Harley
*/

#include <QGuiApplication>

#include "GraphicsEngine.h"

int main(int argc, char *argv[])
{
	QGuiApplication app(argc, argv);

	// set multi-sampling
	QSurfaceFormat *format = new QSurfaceFormat();
	format->setSamples(16);

	// setup window
	os::GraphicsEngine  *window = new os::GraphicsEngine();
	window->setFormat(*format);
	window->resize(1600, 900);
	window->show();

	// animate geometry
	window->setAnimating(true);

	return app.exec();
}
