/**	\file Collider.h
Colliders
*/
#include "Collider.h"

/** Gets the radius of the collider
	\returns The radius of the collider
*/
GLfloat os::Collider::getRadius(){
	return colliderProperties.radius;
}

/** Sets the radius of the collider
	\param rad The new radius of the collider
*/
void os::Collider::setRadius(GLfloat radius){
	colliderProperties.radius = radius;
}

/** Gets the max and min of the AABB collider
	\returns The max and min of the AABB collider
*/
os::AABB os::Collider::getAABB(){
	return colliderProperties.aabb;
}

/** Sets the max and min of the AABB collider
	\param aabb The new max and min of the AABB collider
*/
void os::Collider::setAABB(AABB aabb){
	colliderProperties.aabb = aabb;
}

/**	Gets the collision type of the collider
	\returns The collision type of the collider
*/
os::CollisionType os::Collider::getCollisionType(){
	return colliderProperties.collisionType;
}

/**	Sets the collision type of the collider
	\param type The new collision type of the collider
*/
void os::Collider::setCollisionType(os::CollisionType type){
	colliderProperties.collisionType = type;
}

/** Finds the radius of the mesh
	\param obj The mesh we are finding the radius for
*/
void os::Collider::FindRadius(Mesh *obj)
{
	obj->collider->colliderProperties.aabb.max = QVector3D(0, 0, 0);
	obj->collider->colliderProperties.aabb.min = QVector3D(0, 0, 0);

	for (int i = 0; i < obj->Vertices.count(); i++)
		if (obj->collider->colliderProperties.radius < obj->Vertices[i].length())
			obj->collider->colliderProperties.radius = obj->Vertices[i].length();
}

/** Find AABB of the points
	We find these points by finding the max and min vertices.

	\todo might work... might want to test it out though
	\param obj The mesh we are finding the radius for
*/
void os::Collider::FindAABB(Mesh *obj)
{
	obj->collider->colliderProperties.radius = 0;

	// find max
	for (int i = 0; i < obj->Vertices.count(); i++)
		if (obj->collider->colliderProperties.aabb.max.length() < obj->Vertices[i].length() &&
			(obj->collider->colliderProperties.aabb.max.x() < obj->Vertices[i].x()) &&
			(obj->collider->colliderProperties.aabb.max.y() < obj->Vertices[i].y()) &&
			(obj->collider->colliderProperties.aabb.max.z() < obj->Vertices[i].z()))
			obj->collider->colliderProperties.aabb.max = obj->Vertices[i];

	//find min
	QVector3D oppositeVector(-obj->collider->colliderProperties.aabb.max);
	for (int i = 0; i < obj->Vertices.count(); i++)
		if (oppositeVector.distanceToPoint(obj->Vertices[i]) < oppositeVector.distanceToPoint(obj->collider->colliderProperties.aabb.min) &&
			(obj->collider->colliderProperties.aabb.min.x() > obj->Vertices[i].x()) &&
			(obj->collider->colliderProperties.aabb.min.y() > obj->Vertices[i].y()) &&
			(obj->collider->colliderProperties.aabb.min.z() > obj->Vertices[i].z()))
			obj->collider->colliderProperties.aabb.min = obj->Vertices[i];
}