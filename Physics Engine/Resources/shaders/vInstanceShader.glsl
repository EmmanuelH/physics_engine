#version 430

layout (location = 0) in vec3 position;
layout (location = 1) in vec4 color;

layout (location = 2) in mat4 model_matrix;

uniform mat4 view_matrix;
uniform mat4 projection_matrix;

out vec4 vs_fs_color;

void main(void)
{
    vs_fs_color = color;
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(position, 1.0f);
}